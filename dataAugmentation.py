# Script per augmentar el conjunt etiquetat
import albumentations as A
import cv2
from matplotlib import pyplot as plt

BOX_COLOR = (255, 0, 0) # Red
TEXT_COLOR = (255, 255, 255) # White
 
 
def visualize_bbox(img, bbox, class_name, color=BOX_COLOR, thickness=2):
    """Visualizes a single bounding box on the image"""
    # print("box: " + str(bbox))
    x_min, x_max, y_min, y_max = bbox
 
    cv2.rectangle(img, (x_min, y_min), (x_max, y_max), color=color, thickness=thickness)
 
    ((text_width, text_height), _) = cv2.getTextSize(class_name, cv2.FONT_HERSHEY_SIMPLEX, 0.35, 1)
    cv2.rectangle(img, (x_min, y_min - int(1.3 * text_height)), (x_min + text_width, y_min), BOX_COLOR, -1)
    cv2.putText(
        img,
        text=class_name,
        org=(x_min, y_min - int(0.3 * text_height)),
        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
        fontScale=0.35,
        color=TEXT_COLOR,
        lineType=cv2.LINE_AA,
    )
    return img
 
def yolo_to_opencv(box, dh, dw):
    x,y,w,h = box
    l=int((x-w/2)*dw)
    r=int((x+w/2)*dw)
    t=int((y-h/2)*dh)
    b=int((y+h/2)*dh)
    if l <0:
        l=0
    if r>dw-1:
        r=dw-1
    if t<0:
        t=0
    if b>dh-1:
        b=dh-1
    return [l,r,t,b]

def visualize(image, bboxes,  category_id_to_name):
    # Els bboxes tenen com a 5è element l'etiqueta
    img = image.copy()
    dh, dw, _ = img.shape
    for caixa in bboxes:
        img = visualize_bbox(img, yolo_to_opencv(caixa[0:4],dh,dw), category_id_to_name[caixa[4]])
    plt.figure(figsize=(12, 12))
    plt.axis('off')
    plt.imshow(img)

path_imatge = 'dataset/original_40_100_1_1/obj_train_data/Waterpolo_semifinal_400_040.jpg'
path_boxes ='dataset/original_40_100_1_1/obj_train_data/Waterpolo_semifinal_400_040.txt'
category_id_to_name= {0:'person',1:'WP_player',2:'WP_ball',3:'WP_goallie'}
image = cv2.imread(path_imatge)
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
flab = open(path_boxes,'r')
linies = flab.readlines()
llistaBoxes=[]; llistaLabels=[]
flab.close()
for linia in linies:
    separem = linia.split(' ')
    llistaBoxes.append([float(separem[1]), float(separem[2]), float(separem[3]), float(separem[4].strip()),int(separem[0])])
    llistaLabels.append(int(separem[0]))

transform = A.Compose(
    [A.HorizontalFlip(p=1),
     # A.CLAHE(p=1),
     # A.ColorJitter(p=1),
     # A.Downscale(p=1),
     # A.Equalize(p=1),
     # A.FancyPCA(p=1),
     # A.GaussianBlur(p=1),
     # A.GaussianNoise(p=1),
     # A.GlassBlur(p=1),
     # A.HueSaturationValue(p=1),
     # A.ISONoise(p=1),
     # A.MedianBlur(p=1),
     # A.MotionBlur(p=1),
     # A.MultiplicativeNoise(p=1),
     # A.Normalize(p=1),
     # A.Posterize(p=1) ,
     # A.RandomBrightnessContrast(p=1),
     # A.RandomRain(p=1),
     # A.RandomShadow(p=1),
     # A.RandomSunFlare(p=1),
     # A.Solarize(p=1),
     # A.ToSepia(p=1),
     # A.ToSepia(p=1),
     # A.Rotate(limit=20,p=1),
     A.ShiftScaleRotate(rotate_limit=20,p=1),
    ],
    bbox_params=A.BboxParams(format='yolo'))
transformed = transform(image=image, bboxes=llistaBoxes)
visualize(
    transformed['image'], transformed['bboxes'] ,category_id_to_name)
plt.show()
