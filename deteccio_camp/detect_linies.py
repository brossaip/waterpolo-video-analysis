# PRovant la detecció de línies del opencv per detectar el camp
# Un cop trobades línies, puc aplicar el fitline per utilitzar el coneixement del camp. Detectar línies per colors. Usant el HSL per determinar el color i després el rang, més senzill que en RGB https://stackoverflow.com/questions/35866411/opencv-how-to-detect-lines-of-a-specific-colour
import cv2
import numpy as np
import matplotlib
from matplotlib.pyplot import imshow
from matplotlib import pyplot as plt
from random import randint


def trobem_linies(blur_gray, mascara):
    # Potser el que necessito és un bounding rect rotated https://docs.opencv.org/3.4/de/d62/tutorial_bounding_rotated_ellipses.html per aproximar la corxera
    # Passem el fitlrat a binari i li apliquem el fitline als contours
    filtrat = cv2.bitwise_and(blur_gray, blur_gray, mask=mascara)
    ret, thres = cv2.threshold(filtrat, 17, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thres, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    segments = []
    for contour in contours:
        if len(contour) > 50:
            # approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
            # cv2.drawContours(blur_gray, [approx], 0, (0, 0, 0), 5)
            [vx, vy, x, y] = cv2.fitLine(contour, cv2.DIST_L2, 0, 0.01, 0.01)
            b_rectafit = int((-x * vy / vx) + y)  # És la b de l'equació de la recta
            m_rectafit = vy / vx
            (cx, cy), radius = cv2.minEnclosingCircle(contour)
            # cv2.circle(filtrat, center=(int(cx), int(cy)), radius=int(radius), color=(255, 10, 180), thickness=5)
            a = 1 + m_rectafit * m_rectafit
            b = 2 * m_rectafit * b_rectafit - 2 * cx - 2 * m_rectafit * cy
            c = cx * cx + cy * cy + b_rectafit * b_rectafit - 2 * b_rectafit * cy - radius * radius
            x1 = (-b + np.sqrt(b * b - 4 * a * c)) / (2 * a)
            x2 = (-b - np.sqrt(b * b - 4 * a * c)) / (2 * a)
            y1 = m_rectafit * x1 + b_rectafit
            y2 = m_rectafit * x2 + b_rectafit
            # Per trobar el punt que passa pel centre és utilitzant els moments (https://www.pyimagesearch.com/2016/02/01/opencv-center-of-contour/)
            # M = cv2.moments(contour)
            # cX = int(M["m10"] / M["m00"])
            # cY = int(M["m01"] / M["m00"])
            # try:
            #     cv2.line(img, (int(x_inters_baix), int(y_inters_baix)), (int(x_inters_dalt), int(y_inters_dalt)),
            #              (0, 0, 25), 2)
            # except ValueError:
            #     print("Error al dibuixar area: " + str(box))

            # fem un segon filtre tenint en compte el pendent. Ara buscaré horitzontals, si busqués paral·leles hauria d'afegir un altre criter
            if m_rectafit < 1 and m_rectafit > -1:
                segments.append((x1, y1, x2, y2, m_rectafit, b_rectafit))

    return (segments)


#img = cv2.imread('Waterpolo_semifinal_400_041.jpg')
#img = cv2.imread('Waterpolo_semifinal_400_084.jpg')
img = cv2.imread('./fotoswp/Waterpolo_Echeyde_001000_007.jpg')
# He d'afinar un altre cop els filtres de colors per aquesta nova imatge
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

kernel_size = 5
blur_gray = cv2.GaussianBlur(gray, (kernel_size, kernel_size), 0)

image_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

# color aigua piscina
lower = np.uint8([90, 60, 110])
upper = np.uint8([105, 190, 240])
piscina_mask = cv2.inRange(image_hsv, lower, upper)

# Filtrarem només el requadre de la piscina per evitar les línies de corxeres que no estiguin allà
kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (50, 50))
closing = cv2.morphologyEx(piscina_mask, cv2.MORPH_CLOSE, kernel)
contours, hierarchy = cv2.findContours(closing, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
contours_poly = [None] * len(contours)
boundRect = [None] * len(contours)
centers = [None] * len(contours)
radius = [None] * len(contours)
for i, c in enumerate(contours):
    contours_poly[i] = cv2.approxPolyDP(c, 3, True)
    boundRect[i] = cv2.boundingRect(contours_poly[i])
    centers[i], radius[i] = cv2.minEnclosingCircle(contours_poly[i])
# Busquem el que té el rectangle més gran
indMaxRec = radius.index(max(radius))
piscina_rect_mask = np.zeros(img.shape[:2], dtype="uint8")
# cv2.rectangle(img, (int(boundRect[indMaxRec][0]), int(boundRect[indMaxRec][1])),
#               (int(boundRect[indMaxRec][0] + boundRect[indMaxRec][2]),
#                int(boundRect[indMaxRec][1] + boundRect[indMaxRec][3])), (255, 30, 20), 5)
cv2.rectangle(piscina_rect_mask, (int(boundRect[indMaxRec][0]), int(boundRect[indMaxRec][1])),
              (int(boundRect[indMaxRec][0] + boundRect[indMaxRec][2]),
               int(boundRect[indMaxRec][1] + boundRect[indMaxRec][3])), 255, -1)
img_piscina = cv2.bitwise_and(img, img, mask=piscina_rect_mask)

# Un cop ja tinc la piscina busco les corxeres a aquesta part de la piscina
# Buscarem primer les línies grogues, les vermelles (em donaran com a màxim 3 punts, si es veuen les dues grogues tindria més punts. Estudiar els diferents casos). Com que necessito 4 punts per l'homografia hauré de detectar també les corxeres blanques.
# Hem de tenir en compte també, que tenia el Yolo entrenat per detectar corxeres i porteria. Potser podria barrejar la informació dels dos llocs. O potser per colors i rectes ja en tinc suficient i no caldria fer el Yolo, tenir-lo només per menys etiquetes.
lower = np.uint8([170, 170, 170])
upper = np.uint8([180, 255, 255])
red_mask = cv2.inRange(image_hsv, lower, upper)

# yellow mask
lower = np.uint8([10, 45, 100])
upper = np.uint8([45, 100, 210])
yellow_mask = cv2.inRange(image_hsv, lower, upper)

# combine the mask
mask = cv2.bitwise_or(red_mask, yellow_mask)
filtrat = cv2.bitwise_and(blur_gray, blur_gray, mask=mask)
result = filtrat.copy()

# Filtrem colors amb rectangle piscina i obtenim les línies
groc_piscina_mask = cv2.bitwise_and(piscina_rect_mask, yellow_mask)
linies_groc = trobem_linies(gray, groc_piscina_mask)
for linia in linies_groc:
    try:
        cv2.line(image_hsv, (int(linia[0]), int(linia[1])), (int(linia[2]), int(linia[3])),
                 (randint(0, 255), randint(0, 255), randint(0, 255)), 2)
    except ValueError:
        print(f"Linia error {linia}")

# Creem una matriu per veure les distàncies entre les rectes resultants per fusionar-les
matriu_dist = np.zeros((len(linies_groc), len(linies_groc)))
for k in range(len(linies_groc)):
    r1 = (linies_groc[k][4], linies_groc[k][5])
    P1r1 = (linies_groc[k][0], linies_groc[k][1])
    P2r1 = (linies_groc[k][2], linies_groc[k][3])
    for l in range(len(linies_groc)):
        # Són 4 punts que he d'avaluar, buscant les x les seves y i al revés
        dist = 0
        # La primera x de r2 avaluada a r1
        y_P1r2 = r1[0] * linies_groc[l][0] + r1[1]
        dist = dist + np.abs(y_P1r2 - linies_groc[l][1])
        # La segona x de r2 avaluada a r1
        y_P2r2 = r1[0] * linies_groc[l][2] + r1[1]
        dist = dist + np.abs(y_P2r2 - linies_groc[l][3])
        # La primera y de r2 avaluada a r1
        x_P1r2 = (linies_groc[l][1] - r1[1]) / r1[0]
        dist = dist + np.abs(x_P1r2 - linies_groc[l][0])
        # La primera y de r2 avaluada a r1
        x_P2r2 = (linies_groc[l][3] - r1[1]) / r1[0]
        dist = dist + np.abs(x_P2r2 - linies_groc[l][2])
        matriu_dist[k][l] = dist / 4
# Amb això he comprovat que els segments pertanyen a la mateixa recta. Però jo vull veure si els segments són iguals per poder-los agrupar.
# He de calcular les distàncies entre segments i si aquestes distàncies són petites i tenen el mateix pendent, aleshores són iguales

roig_pisicna_mask = cv2.bitwise_and(piscina_rect_mask, red_mask)
linies_roig = trobem_linies(image_hsv, roig_pisicna_mask)

# Per filtrar els blancs, puc mirar de detectar la porteria amb la línia vertical. Com que el pal és més gruixut, amb un tancament (o obertura, no sé quin) es podria eliminar la xarxa. També és una recta vertical, amb un pendent mol diferent al de les corxeres grogues
