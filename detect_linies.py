# PRovant la detecció de línies del opencv per detectar el camp
# Un cop trobades línies, puc aplicar el fitline per utilitzar el coneixement del camp. Detectar línies per colors. Usant el HSL per determinar el color i després el rang, més senzill que en RGB https://stackoverflow.com/questions/35866411/opencv-how-to-detect-lines-of-a-specific-colour
import cv2
import numpy as np
import matplotlib
from matplotlib.pyplot import imshow
from matplotlib import pyplot as plt

img = cv2.imread('Waterpolo_semifinal_400_041.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

kernel_size = 5
blur_gray = cv2.GaussianBlur(gray, (kernel_size, kernel_size), 0)

image = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
lower = np.uint8([170, 140, 170])
upper = np.uint8([180, 255, 255])
red_mask = cv2.inRange(image, lower, upper)
# yellow color mask
lower = np.uint8([22, 200, 200])
upper = np.uint8([32, 255, 255])
yellow_mask = cv2.inRange(image, lower, upper)
# combine the mask
mask = cv2.bitwise_or(red_mask, yellow_mask)
filtrat = cv2.bitwise_and(blur_gray, blur_gray, mask=mask)
result = filtrat.copy()

# Fem un erode per obtenir l'estructura bàsica
height, width = mask.shape
skel = np.zeros([height, width], dtype=np.uint8)  #[height,width,3]
kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
temp_nonzero = np.count_nonzero(mask)
while (np.count_nonzero(mask) != 0):
    eroded = cv2.erode(mask, kernel)
    cv2.imshow("eroded", eroded)
    temp = cv2.dilate(eroded, kernel)
    cv2.imshow("dilate", temp)
    temp = cv2.subtract(mask, temp)
    skel = cv2.bitwise_or(skel, temp)
    mask = eroded.copy()

# Aproximem trobant els contours i un approxPolyDP
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE,
                                       cv2.CHAIN_APPROX_NONE)
for contour in contours:
    approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True),
                              True)
    cv2.drawContours(img, [approx], 0, (0, 0, 0), 5)

#Second, process edge detection use Canny.
low_threshold = 50
high_threshold = 150
edges = cv2.Canny(filtrat, low_threshold, high_threshold)

# Potser el que necessito és un bounding rect rotated https://docs.opencv.org/3.4/de/d62/tutorial_bounding_rotated_ellipses.html per aproximar la corxera
# Passem el fitlrat a binari i li apliquem el fitline als contours
kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (30, 30))
dilate = cv2.dilate(filtrat, kernel)
dilate = cv2.dilate(dilate, kernel)
ret, thres = cv2.threshold(dilate, 17, 255, cv2.THRESH_BINARY)
contours, hierarchy = cv2.findContours(thres, cv2.RETR_TREE,
                                       cv2.CHAIN_APPROX_NONE)
for contour in contours:
    if len(contour) > 40:
        approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True),
                                  True)
        cv2.drawContours(img, [approx], 0, (0, 0, 0), 5)
        [vx, vy, x, y] = cv2.fitLine(contour, cv2.DIST_L2, 0, 0.01, 0.01)
        lefty = int((-x * vy / vx) + y)  # És la b de l'equació de la recta

        righty = int(((cols - x) * vy / vx) + y)
        cv2.line(img, (cols - 1, righty), (0, lefty), (0, 255, 0), 2)

#Then, use HoughLinesP to get the lines. You can adjust the parameters for better performance.
rho = 1  # distance resolution in pixels of the Hough grid
theta = np.pi / 180  # angular resolution in radians of the Hough grid
threshold = 15  # minimum number of votes (intersections in Hough grid cell)
min_line_length = 50  # minimum number of pixels making up a line
max_line_gap = 20  # maximum gap in pixels between connectable line segments
line_image = np.copy(img) * 0  # creating a blank to draw lines on

# Run Hough on edge detected image
# Output "lines" is an array containing endpoints of detected line segments
lines = cv2.HoughLinesP(edges, rho, theta, threshold, np.array([]),
                        min_line_length, max_line_gap)

for line in lines:
    for x1, y1, x2, y2 in line:
        cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 5)

# Draw the lines on the  image

lines_edges = cv2.addWeighted(img, 0.8, line_image, 1, 0)
plt.imshow(cv2.cvtColor(segmented_image, cv2.COLOR_HSV2RGB))
plt.show()

# Detectant per línies detectava moltes. Potser les he de fer molt petites perquè quedi una línia o he de detectar figures i aproximar les línies com a l'article https://dev.to/simarpreetsingh019/detecting-geometrical-shapes-in-an-image-using-opencv-4g72. Potser només faria falta treballar els paràmetres de la línia, agrupar-les totes i fer un k-nearest neighbour

# Per calcular el pendent és Dy/Dx i b=y-mx
m_grup = []
b_grup = []
for linia in lines:
    if linia[0][0] == linia[0][2]:
        m = (linia[0][1] - linia[0][3]) / (linia[0][0] - 0.9 * linia[0][2])
    else:
        m = (linia[0][1] - linia[0][3]) / (linia[0][0] - linia[0][2])
    b = linia[0][1] - m * linia[0][0]
    m_grup.append(m)
    b_grup.append(b)

############################################
# Algoritme self-taught K-nearest neighbour
############################################
# Agafo un punt inicial i el faig centre del grup. Començarem per la mitjana
m_mitja = np.mean(m_grup)
b_mitja = np.mean(b_grup)
m_std = np.std(m_grup)
b_std = np.std(b_grup)
centroides = [[m_std, b_std]]
mitja_centroides = [[m_std, b_std]]
pesos_mitja_centroides = [1]
# Agafo el següent punt, si està el pendent |m_punt-m_centre|<2  i |b_punt-b_centre|<1000 és del mateix grup, per tant faig el promig dels dos per assignar el nou valor al centroide. El promig incremental és (https://math.stackexchange.com/questions/106700/incremental-averageing) m_n = m_{n-1} + \frac{a_n - m_{n-1}}{n}
for m, b in zip(m_grup, b_grup):
    dist_min = np.finfo(float).max
    comptador = 0
    for centroide in centroides:
        dist_centroide = (m - centroide[0])**2 + (b - centroide[1])**2
        if dist_min > dist_centroide:
            dist_min = dist_centroide
            id_centroide = comptador
        comptador = comptador + 1
    # Mirem si la distància al centroide més proper fa que sigui un nou centroide o no
    m_centroide = centroides[id_centroide][0]
    b_centroide = centroides[id_centroide][1]
    dist_m = abs(m - m_centroide)
    dist_b = abs(b - b_centroide)
    if dist_m < m_std and dist_b < b_std:
        # Pertany al mateix centroide, és del mateix grup
        # Actualitzem la mitja
        pesos_mitja_centroides[
            id_centroide] = pesos_mitja_centroides[id_centroide] + 1
        centroides[id_centroide][0] = m_centroide + (
            m - m_centroide) / pesos_mitja_centroides[id_centroide]
        centroides[id_centroide][1] = b_centroide + (
            b - b_centroide) / pesos_mitja_centroides[id_centroide]
    else:
        # Fem un nou centroide
        centroides.append(m, b)
# Si no es compleix l'anterior és un nou centroide.
# Agafo el nou punt i miro a quin centroide està més a prop. Si del més a prop compleix les condicions anteriors serà un nou punt o un nou centroide
